For start change paths in config.json(.txt) and change path in loadConfig.py.
Then run 'sudo python setup.py install' in the console.

Parametres: 

run in default mode , application removes file or directory: 

    rmpy [path to file] 

To restore the file, use the -r flag: 

    rmpy [path to file] -r 

To remove by regular expression use -c flag: 

    rmpy [path to dir] -c [regular expression] 

If you want the application to ask for permission before any action performed, specidy -i flag: 

    rmpy [path to file] -i

To watch how the application works without any changes being done: 
 
    rmpy [path to fil] --dry_run 

If you don't want to get any console output, specify --silent flag: 

    rmpy [path to file] --silent 

To clear trash by size: 
 
    rmpy [path to file] --size [size] 

To clear trash by time:

    rmpy [path to file] --day [day] 

If you want to load config, write:

    rmpy [path to file] --config [format] [adress] 

Config example:

for json:
{
    "trash": "/home/servan/Programming/lab2/Trash/",
    "log": "/home/servan/Programming/lab2/myrm/files/logs.py",
    "info": "/home/servan/Programming/lab2/myrm/files/info.txt",
    "log_level": "DEBUG",
    "silent": "False",
    "dry_run": "False",
    "auto_trash_clean": "true",
    "trash_size": "1000000",
    "policy": "size",
    "policy_day": "5",
    "policy_size": "100000"
}

for txt:
    trash = /home/servan/Programming/lab2/Trash/
    log = /home/servan/Programming/lab2/files/logs.py
    info = /home/servan/Programming/lab2/myrm/files/info.txt
    log_level = DEBUG
    silent = False
    dry_run = False
    auto_trash_clean = true
    trash_size = 100000
    policy = size
    policy_day = 7
    policy_size = 100000
    
    __________________________________
    
    Explanations:
    trash - path to trash
    log - path to log
    info - path to info
    log_level - level of logging(DEBUG, INFO, WARNING, ERROR, CRITICAL)
    silent - silent mode (true\false)
    dry_run - dry-run mode (true\false)
    auto_trash_clean - auto clean trash mode (true\false)
    trash_size - max trash size (in bits)
    policy - type of auto clean policy (day, size, all)
    policy_day - day of the week for auto clean (monday is 1 and sunday is 7)
    policy_size - if size of trash would be greater than this value, trash automaticaly clean
