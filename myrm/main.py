from myrm import *

def main(args):
    """Entry point here."""

    #Default value
    config_path = DEFAULT_CONFIG    
    log = DEFAULT_LOG

    if args.conf:
        config_path = args.conf

    config = make_new_config(config_path)
    trash_path = config['trash']
    log_level = config['log_level']
    silent = config['silent']
    dry = config['dry_run']
    auto_trash_clean = config['auto_trash_clean']
    max_size = int(config['trash_size'])
    policy = config['policy']
    policy_day = int(config['policy_day'])
    policy_size = int(config['policy_size'])

        
    #if not os.path.exists(os.path.expanduser(trash)):
        #os.makedirs(os.path.expanduser(trash))
   

    if args.confirmation_dialog:
        confirmation_dialog()
    if args.silent:
        silent = True
    if args.dry_run:
        dry = True
    if args.auto_clean_trash:
        auto_trash_clean = True
    if args.max:
        max_size = int(args.max)
    if args.policy:
        policy = args.policy
    if args.size:
        policy_size = int(args.size)
    if args.day:
        policy_day = int(args.day)
    if args.trash:
        trash_path = args.trash


    set_log_settings(log_level, log, silent)


    if args.path and not args.regex:
        if dry:
            for i in args.path:
                if os.path.exists(i):
                    dry_message(''.join(["\"", i, "\" will be moved to trash"]), silent)
                else:
                    dry_message(''.join(["\"", i, "\" will not be moved to trash cuz not exist"]), silent)
        else:
            for i in args.path:
                status, error, path = delete(i, trash_path, max_size)
                show_message(silent, path.join(['Move \"', '\" in trash']), status,
                             error)


    if args.regex and not args.list:
            if dry:
                dry_message(args.regex.join(["All files contains \"", "\" would be move to trash"]), silent)
            else:
                status, error, paths = delete_by_regex(args.path, args.regex, trash_path, max_size)

                for path in paths:
                    show_message(silent, path.join(['Move \"',
                                                            '\" in trash']), status, error)


    if args.restore:
        if dry:
            if os.path.exists(os.path.join(trash_path, args.restore)):
                dry_message(''.join(["\"", args.restore, "\" will be restore from trash"]), silent)
            else:
                dry_message(''.join(["\"", args.restore,
                                     "\" will not be restore from trash cuz not exist"]), silent)
        else:
            status, error, restore_path = restore(args.restore, trash_path)
            show_message(silent, ''.join(['Restore File in ', restore_path]), status, error)


    if args.list:
        if args.regex is None:
            show_trash(trash_path, get_trash_list(trash), max_size, int(args.list))
        else:
            show_trash(trash_path, get_trash_list(trash), max_size, int(args.list), args.regex)


    if args.clean_trash:
        if dry:
            dry_message("Trash would be clean", silent)
        else:
            status, error = clean_trash(trash_path)
            show_message(silent, 'Clean trash', status, error)


    status, error = auto_clean_trash(trash_path, policy, policy_day, policy_size, auto_trash_clean)
    if auto_trash_clean:
        show_message(True, 'Auto clean trash', status, error, '')


def main_for_script():
    args = new_parser().parse_args()
    main(args)


if __name__ == '__main__':
    args = new_parser().parse_args()
    main(args)
