import sys
import logging
import os

from enum import Enum

FORMAT = "%(asctime)s [%(levelname)s] %(message)s"
LINE = '--------------------------------------------------------------'
LOGGER = logging.getLogger()

class error(Enum):
    NO_ERROR = 0
    NO_EXIST = 1
    NO_FILE = 2
    NO_PLACE = 3
    UNKNOW = 4
    WRONG_POLICY = 5
    SYSTEM_DIR = 6

class status(Enum):
    SUCCSES = 10
    ERROR = 40
    WARNING = 30
    CANCEL = 0
    FATAL = 50
    
def confirmation_dialog():
    while True:
        decision = raw_input("Are you sure [y/n]\n")
        if decision == 'y':
            return True
        elif decision == 'n':
            sys.exit("Operation Canceled")
        else:
            print 'incorrect'


def dry_message(message, silent):
    if not silent:
        print (message + "\n")


def set_log_settings(level, path, silent):
    try:
        logging.basicConfig(format = FORMAT, filename = path)
        LOGGER.setLevel(level)
        if silent != True:
            console_handler = logging.StreamHandler()
            LOGGER.addHandler(console_handler)    
    except ValueError:
        print "Incorrect level of logging, set to default value(DEBUG)"
        LOGGER.setLevel("DEBUG")


def log(level, error, action, additionally):
    if level != 10:
        message = ' '.join([action, "[", logging.getLevelName(level), ":", error, "]"])
    else:
        message = action
    LOGGER.log(level, message)



def show_message(silent=False, actions='',statusID=0, error=0, additionally=''):
    """Output information about proccess"""

    status = {0:'SUCCSES', 1:'ERROR!', 2:'WARNING!', 3:'CANCEL', 4:'FATAL'}
    errors = {0:'No Errors',
              1:'File or directory not exist!',
              2:'There is no such file in trash',
              3:'No place in trash', 
              4:'Unknow Error',
              5:'Wrong type of policy',
              6:'File not exist or its system directory'}
    log(statusID, errors[error], actions, additionally)	
			
			
	
