"""
This module is an analogue of default rm. It provides methods for deleting and restoring files,
in addition there are a large number of useful functions.
"""

from __future__ import unicode_literals 
import argparse
import os
import os.path
import shutil
import sys
import datetime


from auxiliary_tools import show_message, confirmation_dialog, dry_message, set_log_settings, error, status
from load_config import load_config


DEFAULT_LOG = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'files/logs.txt')
DEFAULT_CONFIG = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'files/config.json')
TRASH = os.path.expanduser('~/Trash/')


def make_trash(trash_path):
    if not os.path.exists(trash_path):
        os.makedirs(trash_path)


def listdir_nohidden(path):
    """Return files in directory except invisible files"""
    for file in os.listdir(path):
        if not file.startswith('.'):
            yield file


def check_is_exist_and_overwriting(name_in_trash, path, info):
    if os.path.exists(path):
        decision = raw_input("File or directory exist, overwriting? y/n\n")
        if decision == 'y':
            if os.path.isdir(path):
                shutil.rmtree(path)
            else:
                os.remove(path)
        else:
            save_info(name_in_trash, path, info)
            sys.exit("Operation Canceled")


def restore(name, trash_path=TRASH):
    """Restore file from trash by moving them in old path"""

    info = os.path.join(trash_path, '.info.txt')
    try:
        old_path = ''
        data = find_line_in_file(name, info)
        if data != '':
            data[1] = data[1].replace('\n', '')
            if not os.path.exists(os.path.dirname(data[1])):
                os.makedirs(os.path.dirname(data[1]))
            check_is_exist_and_overwriting(data[0], data[1], info)
            os.rename(os.path.join(trash_path, data[0]), data[1])
            old_path = data[1]
            return status.SUCCSES.value, error.NO_ERROR.value, old_path
        else:
            return status.WARNING.value, error.NO_FILE.value, old_path
    except Exception:
        return status.FATAL.value, error.UNKNOW.value, old_path


def find_line_in_file(name, info):
    """Find line in file by name and delete it. Return this line"""

    loc_r = 0
    loc_w = 0
    result = ''
    with open(info, 'r+') as file:
        while True:
            line = file.readline()
            if line == '':
                break
            loc_r = file.tell()
            data = line.split(' ')
            if data[0] != name:
                file.seek(loc_w)
                file.write(line)
                loc_w = file.tell()
                file.seek(loc_r)
            elif data[0] == name:
                result = data
        file.truncate(loc_w)
        return result


def save_info(name_in_trash, old_path, info):
    """Save information about deleted file in file."""

    with open(info, 'a') as file:
        file.write(' '.join([name_in_trash, old_path]))
        file.write('\n')


def make_new_name(name, trash_path):
    """Solve problem with same names in directory by adding a number in name"""

    name_num = 1
    tmp_name = name
    while os.path.exists(os.path.join(trash_path, tmp_name)):
        tmp_name = name
        tmp_name = ''.join([tmp_name, "(", str(name_num), ")"])
        name_num += 1
    return tmp_name


def check_system_dir(path):
    """Check is directory system. Return true if directory is system, else return false"""

    path = os.path.realpath(path)
    path = path.split('/')
    if len(path) == 2 and (path[0] == '' or path[1] == ''):
        return True
    else:
        return False


def delete_by_regex(path, regex, trash_path=TRASH, max_size=1000000):
    """
    Move in trash all files(directories) in derectory that contain certain char(s). 
    Return paths of deleted files
    """
    make_trash(trash_path)
    info = os.path.join(trash_path, '.info.txt')
    paths = []
    try:
        if path == []:
            path = [1]
            path[0] = os.getcwd()
        for i in path:
            if check_system_dir(i):
                return status.ERROR.value, error.SYSTEM_DIR.value, ''
            if os.path.isdir(i):
                for file in os.listdir(i):
                    if regex in os.path.basename(file):
                        if not check_size(trash_path, max_size, file):
                            return status.ERROR.value, error.NO_PLACE.value, ''
                        new_name = os.path.basename(file)
                        tmp_name = make_new_name(new_name, trash_path)
                        save_info(tmp_name, '/'.join([os.path.abspath(i), new_name]), info)
                        os.rename('/'.join([i, new_name]), os.path.join(trash_path, tmp_name))
                        paths.append('/'.join([os.path.abspath(i), new_name]))
            else:
                return status.ERROR.value, error.NO_EXIST.value, ''
        return status.SUCCSES.value, error.NO_ERROR.value, paths
    except Exception:
        return status.FATAL.value,  ''


def delete(name, trash_path=TRASH, max_size=1000000):
    """Move in trash file(directory) by name. Return path of deleted file"""
    make_trash(trash_path)
    info = os.path.join(trash_path, '.info.txt')
    try:
        path = os.path.abspath(name)
        if check_system_dir(path):
            return status.ERROR.value, error.SYSTEM_DIR.value, path
        if not check_size(trash_path, max_size, path):
            return status.ERROR.value, error.NO_PLACE.value, path
        if os.path.isfile(path) or os.path.isdir(path) or os.path.islink:
            new_name = os.path.basename(path)
            tmp_name = make_new_name(new_name, trash_path)
            os.rename(path, os.path.join(trash_path, tmp_name))
            save_info(tmp_name, path, info)
        return status.SUCCSES.value, error.NO_ERROR.value, path
    except OSError:
        return status.ERROR.value, error.NO_EXIST.value, path
    except Exception:
        return status.FATAL.value, error.UNKNOW.value, path


def clean_trash(trash_path=TRASH):
    """Delete all files from trash."""

    try:
        for file in os.listdir(trash_path):
            file_for_rm = os.path.join(trash_path, file)
            if os.path.isdir(file_for_rm):
                shutil.rmtree(file_for_rm)
            else:
                os.remove(file_for_rm)
        return status.SUCCSES.value, error.NO_ERROR.value
    except Exception:
        return status.FATAL.value, error.UNKNOW.value


def bool_to_string(string):
    """Convert string to boolean."""

    if string == 'False' or string == 'false':
        return False
    elif string == 'True' or string == 'true':
        return True
    else:
        return False


def make_new_config(path=DEFAULT_CONFIG):
    """Load config from file and create dict with all attributes of config. Return this dict"""

    config = load_config(path)
    trash_path = config["trash"]
    log_level = config["log_level"]
    silent = config["silent"]
    dry = config["dry_run"]
    auto_trash_clean = config["auto_trash_clean"]
    trash_size = config["trash_size"]
    policy = config["policy"]
    policy_day = config["policy_day"]
    policy_size = config["policy_size"]


    silent = bool_to_string(silent)
    dry = bool_to_string(dry)
    auto_trash_clean = bool_to_string(auto_trash_clean)


    data = {'trash':trash_path,
            'log_level':log_level,
            'silent':silent,
            'dry_run':dry,
            'auto_trash_clean':auto_trash_clean,
            'trash_size':trash_size,
            'policy': policy,
            'policy_day':policy_day,
            'policy_size':policy_size}

    return data


def size_of_file(path):
    """Return size of file or directory recursive."""

    size = 0
    for file in listdir_nohidden(path):
        file_for_size = os.path.join(path, file)
        if os.path.isdir(file_for_size):
            size += os.path.getsize(file_for_size) + make_new_config(file_for_size)
        else:
            size += os.path.getsize(file_for_size)
    return size


def check_size(trash_path, max_size, name):
    """Check is have enough space in trash for new file(directory). Return True if enough."""

    file_size = 0
    trash_size = make_new_config(trash_path)
    tmp = max_size - trash_size
    if os.path.isfile(name):
        file_size = os.path.getsize(name)
    elif os.path.isdir(name):
        file_size = 4096 + make_new_config(name)
    if file_size <= tmp:
        return True
    else:
        return False


def auto_clean_trash(trash_path, policy='all', policy_day=7, policy_size=100000, auto_trash_clean=False):
    """
    Auto clean trash by different politics: 
    day - clean trash in one day of the week
    size - clean trash if size more than value
    all - incude both options
    """

    if auto_trash_clean:
        if policy == 'all':
            if datetime.datetime.today().isoweekday() == policy_day:
                clean_trash(trash_path)
            if size_of_file(trash_path) >= policy_size:
                clean_trash(trash_path)
        elif policy == 'day':
            if datetime.datetime.today().isoweekday() == policy_day:
                clean_trash(trash_path)
        elif policy == 'size':
            if size_of_file(trash_path) >= policy_size:
                clean_trash(trash_path)
        else:
            return status.WARNING.value, error.WRONG_POLICY.value
    return status.SUCCSES.value, error.NO_ERROR.value


def get_trash_list(trash_path):
    """Return list of file in trash."""
    return listdir_nohidden(trash_path)


def show_trash(trash_path, trash_list, max_size, num, contain=''):
    """Show trash(all files, number of files or files by regex) as table with informatiom about files."""
    count = 1
    print ''.join(['\n','Trash size(now/max): ', str(make_new_config(trash_path)), '/', str(max_size)])
    print '_________________________________________________________________'
    print '|                                                                |'
    print "| {:<20} {:<20} {:<20} {}".format('Name:', 'Type:', 'Size:', '|');
    print '|________________________________________________________________|'
    print '|                                                                |'
    for file in trash_list:
        if count == num + 1:
            break
        path = os.path.join(trash_path, file)
        if contain in os.path.basename(file):
            if os.path.isfile(path):
                print "| {:<20} {:<20} {:<20} {}".format(file, 'File', os.path.getsize(path), '|')

            if os.path.isdir(path):
                print "| {:<20} {:<20} {:<20} {}".format(file, 'Directory',
                                                         make_new_config(path) + 4096, '|')
            if os.path.islink(path):
                print "| {:<20} {:<20} {:<20} {}".format(file, 'Link', os.path.getsize(path), '|')
            count += 1
    print '|________________________________________________________________|'


def new_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("path", nargs='*', help="Path to file")
    parser.add_argument("-res", dest="restore", help="Restore file")
    parser.add_argument("-c", dest="regex", help="delete by regex")
    parser.add_argument("-e", dest="clean_trash", help="clean trash", action="store_true")
    parser.add_argument("-s", dest="silent", help="silent mode", action="store_true")
    parser.add_argument("-d", dest="dry_run", help="dry_run mode", action="store_true")
    parser.add_argument("-i", dest="confirmation_dialog", help="Confirmation dialog", action="store_true")
    parser.add_argument("-ac", dest="auto_clean_trash", help="Auto clean mode", action="store_true")
    parser.add_argument("--list", help="Show trash", nargs='?', const=-1)
    parser.add_argument("--max", help="Set max trash size")
    parser.add_argument("--conf", help="Set path of ur config")
    parser.add_argument("--trash", help="Set path to trash")
    parser.add_argument("--policy", help="Set policy type")
    parser.add_argument("--size", help="Set policy_size mode value")
    parser.add_argument("--day", help="Set policy_day mode value")
    return parser
