import unittest
import sys
import os



from main import *


trash = os.path.join(os.getcwd(), 'Trash/')


class Test(unittest.TestCase):


    def setUp(self):
        if not os.path.exists(trash):
            os.makedirs(trash)
        os.mkdir("TestDir")
        os.mkdir("TestDir/b")
        files = ["aab","abaa","bb"]
        for file in files:
            with open("TestDir/%s" % file, 'w'):
                pass
            with open("TestDir/b/%s" % file,'w'):
                pass


    def tearDown(self):
        clean_trash(trash)
        if os.path.exists("TestDir"):
            shutil.rmtree("TestDir")


    def test_del_one_file(self):
        file = ["TestDir/aab"]
        delete(file[0], trash, 100000)
        self.assertFalse(os.path.exists(file[0]))
        self.assertTrue(os.path.exists(os.path.join(trash,'aab')))


    def test_del_one_dir(self):
        file = ["TestDir/b"]
        delete(file[0], trash, 100000)
        self.assertFalse(os.path.exists(file[0]))
        self.assertTrue(os.path.exists(os.path.join(trash,'b')))


    def test_delete_files_with_same_name(self):
        file = ["TestDir/aab","TestDir/abaa","TestDir/b/aab","TestDir/b/abaa"]
        for i in file:
            delete(i, trash, 100000)
        self.assertFalse(os.path.exists(file[0]))		
        self.assertFalse(os.path.exists(file[1]))
        self.assertFalse(os.path.exists(file[2]))
        self.assertFalse(os.path.exists(file[3]))
        self.assertTrue(os.path.exists(os.path.join(trash,'aab')))
        self.assertTrue(os.path.exists(os.path.join(trash,'abaa')))
        self.assertTrue(os.path.exists(os.path.join(trash,'aab(1)')))
        self.assertTrue(os.path.exists(os.path.join(trash,'abaa(1)')))


    def test_del_from_different_dirs(self):
        file = ["TestDir/aab","TestDir/b/bb"]
        for i in file:
            delete(i, trash, 100000)
        self.assertFalse(os.path.exists(file[0]))
        self.assertFalse(os.path.exists(file[1]))
        self.assertTrue(os.path.exists(os.path.join(trash,'aab')))
        self.assertTrue(os.path.exists(os.path.join(trash,'bb')))


    def test_del_contain_char(self):
        file = ["TestDir/aab","TestDir/abaa","TestDir/b","TestDir/bb"]
        path = ["TestDir/"]
        contain = 'ab'
        delete_by_regex(path, contain, trash, 100000)
        self.assertFalse(os.path.exists(file[0]))
        self.assertFalse(os.path.exists(file[1]))
        self.assertTrue(os.path.exists(file[2]))
        self.assertTrue(os.path.exists(file[3]))
        self.assertTrue(os.path.exists(os.path.join(trash,'aab')))
        self.assertTrue(os.path.exists(os.path.join(trash,'abaa')))


    def test_del_contain_char_from_different_dirs(self):		
        file = ["TestDir/aab","TestDir/abaa","TestDir/b/aab","TestDir/b/abaa",
                "TestDir/b","TestDir/bb","TestDir/b/bb"]

        path = ["TestDir/","TestDir/b/"]
        regex = 'ab'
        delete_by_regex(path, regex, trash, 100000)
        self.assertFalse(os.path.exists(file[0]))
        self.assertFalse(os.path.exists(file[1]))
        self.assertFalse(os.path.exists(file[2]))
        self.assertFalse(os.path.exists(file[3]))
        self.assertTrue(os.path.exists(file[4]))
        self.assertTrue(os.path.exists(file[5]))
        self.assertTrue(os.path.exists(file[6]))
        self.assertTrue(os.path.exists(os.path.join(trash,'aab')))
        self.assertTrue(os.path.exists(os.path.join(trash,'abaa')))
        self.assertTrue(os.path.exists(os.path.join(trash,'aab(1)')))
        self.assertTrue(os.path.exists(os.path.join(trash,'abaa(1)')))


    def test_restore_file(self):
        file = ["TestDir/aab"]
        delete(file[0], trash, 100000)
        self.assertFalse(os.path.exists(file[0]))
        restore('aab',trash)
        self.assertTrue(os.path.exists(file[0]))


    def test_restore_dir(self):
        file = ["TestDir/b"]
        delete(file[0], trash, 100000)
        self.assertFalse(os.path.exists(file[0]))
        restore('b',trash)
        self.assertTrue(os.path.exists(file[0]))


    def test_trash_size(self):
        file = ["TestDir/aab","TestDir/b"]
        with open("TestDir/aab", 'a') as f:
            f.write("qwerty")
        os.mkdir("TestDir/b/bbvv")
        self.assertEqual(size_of_file(trash),0)
        for i in file:
            delete(i,trash,100000)
        self.assertEqual(size_of_file(trash), 8198)


    def test_clean_trash(self):
        file = ["TestDir/aab", "TestDir/abaa", "TestDir/b"]
        self.assertEqual(size_of_file(trash), 0)
        for i in file:
            delete(i, trash, 100000)
        self.assertNotEqual(size_of_file(trash), 0)
        clean_trash(trash)
        self.assertEqual(size_of_file(trash), 0)


    def test_size_for_file(self):
        self.assertFalse(check_size(trash, 4095, "TestDir/b"))
        self.assertTrue(check_size(trash, 4097, "TestDir/b"))
        self.assertTrue(check_size(trash, 4096, "TestDir/b"))


    def test_restor_file_in_not_exist_dir(self):
        file = "TestDir/b/bb"
        self.assertTrue(os.path.exists(file))
        delete(file ,trash, 100000)
        self.assertFalse(os.path.exists(file))
        shutil.rmtree("TestDir/b")
        self.assertFalse(os.path.exists("TestDir/b"))
        restore('bb', trash)
        self.assertTrue(os.path.exists("TestDir/b"))
        self.assertTrue(os.path.exists(file))
            

    def test_ultimate_count_of_files(self):
        n = 10
        dirr = "TestDir/"
        for i in range(0,n):
            with open("TestDir/%s" % str(i), 'w'):
                pass
        for i in range(0,n):
            delete(os.path.join(dirr, str(i)),trash,1000000)
        for i in range(0,n):
            self.assertTrue(os.path.exists(os.path.join(trash, str(i))))
        for i in range(0,n):
            restore(str(i), trash)
        for i in range(0,n):
            self.assertTrue(os.path.exists(os.path.join(dirr, str(i))))


    def test_args_delete_one_file(self):
        file = "TestDir/aab"
        args = new_parser().parse_args()
        args.silent = True
        args.path.append(file)
        args.trash = trash	
        main(args)
        self.assertFalse(os.path.exists(file))
        self.assertTrue(os.path.exists(os.path.join(trash, 'aab')))


    def test_args_delete_one_dir(self):
        file = "TestDir/b"
        args = new_parser().parse_args()
        args.silent = True
        args.path.append(file)
        args.trash = trash	
        main(args)
        self.assertFalse(os.path.exists(file))
        self.assertTrue(os.path.exists(os.path.join(trash, 'b')))


    def test_args_delete_contain(self):
        file = "TestDir"
        args = new_parser().parse_args()
        args.path.append(file)
        args.silent = True
        args.regex = 'ab'
        args.trash = trash	
        main(args)
        self.assertFalse(os.path.exists("TestDir/aab"))
        self.assertFalse(os.path.exists("TestDir/abaa"))
        self.assertTrue(os.path.exists(os.path.join(trash, 'aab'))) 
        self.assertTrue(os.path.exists(os.path.join(trash, 'abaa')))


    def test_args_restore(self):
        file = "TestDir/aab"
        args = new_parser().parse_args()
        args.silent = True
        args.path.append(file)
        args.restore = 'aab'
        args.trash = trash	
        main(args)
        self.assertTrue(os.path.exists(file))
        self.assertFalse(os.path.exists(os.path.join(trash, 'aab')))


    def test_args_dry(self):
        file = "TestDir/aab"
        args = new_parser().parse_args()
        args.silent = True
        args.dry_run = True
        args.path.append(file)
        args.trash = trash
        main(args)
        self.assertTrue(os.path.exists(file))
        self.assertFalse(os.path.exists(os.path.join(trash, 'aab')))


    def test_args_clean_trash(self):
        file = "TestDir/aab"
        args = new_parser().parse_args()
        args.silent = True
        args.path.append(file)
        args.clean_trash = True
        args.trash = trash	
        main(args)
        self.assertFalse(os.path.exists(file))
        self.assertFalse(os.path.exists(os.path.join(trash, 'aab')))


    def test_args_auto_clean_trash_day(self):
        file = "TestDir/aab"
        args = new_parser().parse_args()
        args.silent = True
        args.path.append(file)
        args.auto_clean_trash = True
        args.policy = 'day'
        args.day = datetime.datetime.today().isoweekday()
        args.trash = trash
        main(args)
        self.assertFalse(os.path.exists(file))
        self.assertFalse(os.path.exists(os.path.join(trash, 'aab')))


    def test_args_auto_clean_trash_size(self):
        file = "TestDir/b"
        args = new_parser().parse_args()
        args.silent = True
        args.path.append(file)
        args.auto_clean_trash = True
        args.policy = 'size'
        args.size = 100
        args.trash = trash
        main(args)
        self.assertFalse(os.path.exists(file))
        self.assertFalse(os.path.exists(os.path.join(trash, 'b')))  

   # def test_args_clean_trash(self)
        
        

if __name__ == '__main__':
	unittest.main()

