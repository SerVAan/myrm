import json
import os


DEFAULT_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'files/config.json')


def set_default_config():
    trash_path = os.path.expanduser('~/Trash/')
    data = {'trash':trash_path,
            'log_level':'DEBUG',
            'silent':'False',
            'dry_run':'False',
            'auto_trash_clean':'False',
            'trash_size':'1000000',
            'policy': 'all',
            'policy_day':'1',
            'policy_size':'100000'}
    with open (DEFAULT_PATH, 'w') as file:
        json.dump(data, file, indent=2)
    return data
    


def load_config(path=DEFAULT_PATH):
    if os.path.exists(path):
        extension = os.path.splitext(os.path.basename(path))[1]
        if extension == '.json':
            with open(path) as jsonFile:
                data = json.load(jsonFile)
                return data
        elif extension == '.txt':
            data = {}
            with open(path,'r') as txtFile:
                lines = txtFile.readlines()
                for line in lines:
                    string = line.split('=')[1].lstrip(' ').replace('\n','')
                    data[line.split('=')[0].rstrip(' ')] = string
                return data
    else:
        print "Config error, set default config"
        data = set_default_config()
        return data				
	



