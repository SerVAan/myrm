from setuptools import setup, find_packages
from os.path import join, dirname


setup(
    name='myrm',
    version='1.0',
    include_package_data=True,
    #packages=find_packages(),
    packages=['myrm', 'myrm.files'],
    package_data={'':['config.json', 'config.txt', 'info.txt', 'logs.txt']},
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    entry_points={            
            'console_scripts':
                ['myrm = myrm.main:main_for_script']
            }
)
